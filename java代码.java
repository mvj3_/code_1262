图2效果：改变了对话框的图表，添加了三个按钮

Dialog dialog = new AlertDialog.Builder(this).setIcon(
     android.R.drawable.btn_star).setTitle("喜好调查").setMessage(
     "你喜欢李连杰的电影吗？").setPositiveButton("很喜欢",
     new OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
       // TODO Auto-generated method stub
       Toast.makeText(Main.this, "我很喜欢他的电影。",
         Toast.LENGTH_LONG).show();
      }
     }).setNegativeButton("不喜欢", new OnClickListener() {

    @Override
    public void onClick(DialogInterface dialog, int which) {
     // TODO Auto-generated method stub
     Toast.makeText(Main.this, "我不喜欢他的电影。", Toast.LENGTH_LONG)
       .show();
    }
   }).setNeutralButton("一般", new OnClickListener() {

    @Override
    public void onClick(DialogInterface dialog, int which) {
     // TODO Auto-generated method stub
     Toast.makeText(Main.this, "谈不上喜欢不喜欢。", Toast.LENGTH_LONG)
       .show();
    }
   }).create();

   dialog.show();

 

图3效果：信息内容是一个简单的View类型

new AlertDialog.Builder(this).setTitle("请输入").setIcon(
     android.R.drawable.ic_dialog_info).setView(
     new EditText(this)).setPositiveButton("确定", null)
     .setNegativeButton("取消", null).show();
图4效果：信息内容是一组单选框

new AlertDialog.Builder(this).setTitle("复选框").setMultiChoiceItems(
     new String[] { "Item1", "Item2" }, null, null)
     .setPositiveButton("确定", null)
     .setNegativeButton("取消", null).show();

图5效果：信息内容是一组多选框

new AlertDialog.Builder(this).setTitle("单选框").setIcon(
     android.R.drawable.ic_dialog_info).setSingleChoiceItems(
     new String[] { "Item1", "Item2" }, 0,
     new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
       dialog.dismiss();
      }
     }).setNegativeButton("取消", null).show();

图6效果：信息内容是一组简单列表项

new AlertDialog.Builder(this).setTitle("列表框").setItems(
     new String[] { "Item1", "Item2" }, null).setNegativeButton(
     "确定", null).show();

图7效果：信息内容是一个自定义的布局

1.布局文件

<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
 android:layout_height="wrap_content" android:layout_width="wrap_content"
 android:background="#ffffffff" android:orientation="horizontal"
 android:id="@+id/dialog">
 <TextView android:layout_height="wrap_content"
   android:layout_width="wrap_content"
  android:id="@+id/tvname" android:text="姓名：" />
 <EditText android:layout_height="wrap_content"
  android:layout_width="wrap_content" android:id="@+id/etname" android:minWidth="100dip"/>

</LinearLayout>

2.调用代码

LayoutInflater inflater = getLayoutInflater();
   View layout = inflater.inflate(R.layout.dialog,
     (ViewGroup) findViewById(R.id.dialog));

   new AlertDialog.Builder(this).setTitle("自定义布局").setView(layout)
     .setPositiveButton("确定", null)
     .setNegativeButton("取消", null).show();
